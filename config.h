/* See LICENSE file for copyright and license details. */
#include "dwm.h"

#define POLYGON_SEL

#ifdef POLYGON_SEL
static const unsigned int sel_cl = 0;
static const unsigned int unsel_cl = 8;

// Define list of points to use as polygon mask for selected and
// unselected clients.
// will have *Client c, l=cl, and ll=cl*2
#define POLYGON { \
      { l, 0       }, \
      { c->w-ll,      0         }, \
      { c->w-ll, c->h-l         }, \
      { l,     c->h-l       }, \
    };
#define POLY_LEN 4

// Can have different polygons for selected and not selected
#define SEL_POLYGON POLYGON
#define N_SEL_POINTS POLY_LEN
#define UNSEL_POLYGON POLYGON
#define N_UNSEL_POINTS POLY_LEN
#endif

//#define TAB_APPS "urxvt zathura feh st-256color termite"
#define TAB_APPS ""

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int seltab    = 10;

static const unsigned int snap      = 32;       /* snap pixel */

#if defined(raspberrypi)
static const unsigned int gappx     = 2;
#else
static const unsigned int gappx     = 10;
#endif

static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Monospace:size=14" };
static const char dmenufont[]       = "Monospace:size=14";
static const char *xres             = "/home/chris/.Xresources";


/* colours */
static char col_fg_nrm[]          = "#dfdfdf";
static char col_bg_nrm[]          = "#191919";
static char col_bd_nrm[]          = "#191919";

static char col_fg_sel[]          = "#dfdfdf";
static char col_bg_sel[]          = "#191919";
static char col_bd_sel[]          = "#dfdfdf";
static char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_fg_nrm,    col_bg_nrm,    col_bd_nrm },
	[SchemeSel]  = { col_fg_sel,    col_bg_sel,    col_bd_sel  },
};

static const Sorting sortings[] = {
  /* bin name, display name, xwindow classes */
  { /* web   */ "爵", "Firefox Surf"},
  { /* CAD   */ "", "Freecad-daily"},
  { /* term  */ "", "URxvt Termite"},
  { /* print */ "", "Slic3r" },
  { /* mail  */ "", "Thunderbird"},
  { /* imged */ "",  "Gimp"},
  { /* doc   */ "",  "Evince zathura libreoffice mupdf"},
  { /* video */ "辶", "MPlayer VLC"}
};
static const char* bin_unsrt = "";

#define MAXTAGLEN 24

#define WEB "firefox"

// host dependant settings
#if defined(archuni)
#define TAGC 8
static const Rule rules[] = {
	/* class              instance    title      tags mask     isfloating   monitor */
  	{ "Thunderbird",    NULL,       NULL,      1 << 1,            0,           0 },
  	{ "MPlayer",        NULL,       NULL,      0,            1,           0 },
  	{ "Matplotlib",     NULL,       NULL,      0,            1,           -1 },
  	{ NULL,             NULL,       "mantemp",      0,            1,           -1 },
  	{ "FLOATING",       NULL,       NULL,      0,            1,           0 },
};
#elif defined(weearch)
#define TAGC 5
static const Rule rules[] = {
	/* class                instance    title      tags mask     isfloating   monitor */
  	{ "Firefox",          NULL,       NULL,      1<<2,            0,           -1 },
  	{ "Surf",             NULL,       NULL,      1<<2,            0,           -1 },
  	{ "Freecad-daily",    NULL,       NULL,      1<<3,            0,           -1 },
  	{ "Slic3r",           NULL,       NULL,      1<<4,            0,           -1 },
  	{ NULL,             NULL,       "mantemp",      0,            1,           -1 },
  	{ "FLOATING",         NULL,       NULL,      0,            1,           0 },
};
#elif defined(archtop)
#define TAGC 8
static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
  	{ NULL,             NULL,       "mantemp",      0,            1,           -1 },
  	{ "FLOATING",    NULL,       NULL,      0,            1,           0 },
};
#elif defined(archhome)
#define TAGC 8
static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
  	{ "Thunderbird",    NULL,       NULL,      1 << 1,            0,           0 },
  	{ "MPlayer",        NULL,       NULL,      0,            1,           0 },
  	{ "Matplotlib",     NULL,       NULL,      0,            1,           -1 },
  	{ NULL,             NULL,       "mantemp",      0,            1,           -1 },
  	{ "FLOATING",    NULL,       NULL,      0,            1,           0 },
};
#else
#define TAGC 8
static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
  	{ NULL,             NULL,       "mantemp",      0,            1,           -1 },
  	{ "FLOATING",    NULL,       NULL,      0,            1,           0 },
};
#endif

static const char* tags[TAGC];

/* layout(s) */
static const float mfact     = 0.6; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

/* dialog locations */
static const int locations[][4] = {{-5,5,640,480}};

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "◰",		tile },    /* first entry is default */
	{ "★",		NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-p", "run:", NULL };
static const char *umntcmd[] = { "dmenu_umnt", "-m", dmenumon, "-p", "unmount:", NULL };
static const char *termcmd[]  = { "/bin/sh", "-c", "$TERMCMD", NULL };
static const char *lockcmd[]  = { "slock", NULL };
static const char *mancmd[]  = { "fman", NULL };
static const char *webcmd[]  = { WEB, NULL };
static const char *upvol[]    = { "/usr/bin/pactl", "set-sink-volume", "0", "+5%",     NULL };
static const char *downvol[]  = { "/usr/bin/pactl", "set-sink-volume", "0", "-5%",     NULL };
static const char *mutevol[]  = { "/usr/bin/pactl", "set-sink-mute", "0", "toggle", NULL };
static const char *munext[]  = { "/usr/bin/mpc", "next", NULL };
static const char *muprev[]  = { "/usr/bin/mpc", "prev", NULL };
static const char *mupause[]  = { "/usr/bin/mpc", "toggle", NULL };

/* key bindings */
static Key keys[] = {
	/* modifier                     key        function        	argument */
  //## General
  //
	{ MODKEY|ShiftMask,             XK_q,      quit,           	{0} },
	{ MODKEY,                       XK_q,      killclient,     	{0} },
	{ MODKEY,                       XK_p,      zoom,            {0} },
	{ MODKEY,                       XK_b,      togglebar,      	{0} },
	{ MODKEY,                       XK_Tab,    view,           	{0} },
	{ MODKEY,                       XK_a,      view,           	{.ui = ~0 } },
 	{ MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },

  //## Floating clients
  //
 	{ MODKEY,                       XK_f,     togglefloating,           {.i = 0 } },
 	{ MODKEY|ShiftMask,             XK_f,     togglefloating,           {.i = 0 } },
 	{ MODKEY|ShiftMask,             XK_f,     setsizeloc,               {.i = 0 } },
 	{ MODKEY|ShiftMask,             XK_f,     tag,                      {.ui = ~0 } },

  //## Spawning commands
  //
	{ MODKEY,                       XK_d,      spawn,          	{.v = dmenucmd } },
	{ MODKEY,                       XK_u,      spawn,          	{.v = umntcmd } },
	{ MODKEY,                       XK_z,      spawn,          	{.v = termcmd } },
	{ MODKEY,                       XK_w,      spawn,          	{.v = webcmd } },
	{ MODKEY,                       XK_s,      spawn,          	{.v = lockcmd } },
	{ MODKEY,                       XK_m,      spawn,          	{.v = mancmd } },
        
  //## Changing focus
  //
	{ MODKEY,                       XK_k,      focusdir,       	{.i = 1 } },
	{ MODKEY,                       XK_l,      focusdir,       	{.i = 2 } },
	{ MODKEY,                       XK_j,      focusdir,       	{.i = 3 } },
	{ MODKEY,                       XK_h,      focusdir,       	{.i = 4 } },

  //## Master area
  //
	{ MODKEY|ShiftMask,             XK_h,      incnmaster,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l,      incnmaster,      {.i = 1 } },
	{ MODKEY|ShiftMask,             XK_k,      setmfact,       	{.f = 0.1 } },
	{ MODKEY|ShiftMask,             XK_j,      setmfact,       	{.f = -0.1 } },

	{ MODKEY,                       XK_grave,  focusmon,       	{.i = 0 } },
	{ MODKEY|ShiftMask,             XK_grave,  tagmon,       	  {.i = 0 } },

  //## Media
  //
  //Media keys are handy for controlling mpd, without the need to open an mpd 
  //client. There are two general schema for this; one using the XF86 media keys,
  //which are handy if the keyboard has them, and the other using the v key.
  //
	{ MODKEY,                       XK_v,      spawn,          	{.v = upvol } },
	{ MODKEY|ShiftMask,             XK_v,      spawn,          	{.v = downvol } },
	{ MODKEY|ControlMask,           XK_v,      spawn,          	{.v = mupause } },
	{ 0,              XF86XK_AudioLowerVolume, spawn,           {.v = downvol } },
	{ 0,              XF86XK_AudioMute,        spawn,           {.v = mutevol } },
  { 0,              XF86XK_AudioRaiseVolume, spawn,           {.v = upvol } },
	{ 0,              XF86XK_AudioNext,        spawn,           {.v = munext } },
	{ 0,              XF86XK_AudioPrev,        spawn,           {.v = muprev } },
  { 0,              XF86XK_AudioPlay,       spawn,            {.v = mupause } },
  { 0,              XF86XK_AudioPause,       spawn,           {.v = mupause } },
  { 0,              XF86XK_AudioStop,       spawn,            {.v = mupause } },

  //## Tags
  //
  //There are N tags in a dwm session, where N is an integer 1-9. The tags can 
  //be accessed by using different modifier keys with the number keys, each
  //representing one of the tags.
  //
  // super-X
  //  : Focus only tag X.
  //
  // control-super-X
  //  : Add tag X to the focus.
  //
  // shift-super-X
  //  : Move window to tag X.
  //
  // control-shift-super-X
  //  : Copy window to tag X.
  //
	TAGKEYS(                        XK_1,                      	0)
	TAGKEYS(                        XK_2,                      	1)
	TAGKEYS(                        XK_3,                      	2)
	TAGKEYS(                        XK_4,                      	3)
	TAGKEYS(                        XK_5,                      	4)
	TAGKEYS(                        XK_6,                      	5)
	TAGKEYS(                        XK_7,                      	6)
	TAGKEYS(                        XK_8,                      	7)
	TAGKEYS(                        XK_9,                      	8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} }
};

