#!/usr/bin/env
import re

with open('config.h') as confighf:
    lines = [line[:-1] for line in confighf.readlines()]

## get variables
vardic = dict()
vardec_re = re.compile(r'(?:static\s+)?(?:const\s+)?(\S+)\s+\*?([\w_]+)\*?\*?\[?\d?\]?\s+=\s+(.*);')
def_re = re.compile(r'#define\s+(\w+)\s+(.*)')
for line in lines:
    if mobj := vardec_re.match(line):
        vardic[mobj.group(2)] = mobj.group(3)
    elif mobj := def_re.match(line):
        vardic[mobj.group(1)] = mobj.group(2)

keydic = dict()
keydic['MODKEY'] = 'super'
keydic['ShiftMask'] = 'shift'
keydic['ControlMask'] = 'control'
for l in 'abcdefghijklmnopqrstuvwxyz':
    keydic[f'XK_{l}'] = l
keydic['XK_grave'] = 'grave'
keydic['XK_Tab'] = 'tab'
for i in range(1, 21):
    keydic[f'XK_F{i}'] = f'F{i}'

descdic = {
        'spawn': 'Run command: ',
        'focusdir': 'Change focus ',
        'focusmon': 'Toggle focussed screen.',
        'tagmon': 'Move client between screens.',
        'quit': 'Quit dwm.',
        'killclient': 'Close selected window.',
        'zoom':'Move client to master area.',
        'togglebar':'Toggle bar on current screen.',
        'view':{
            '':'View last visited tag',
            '.ui = ~0': 'View all tags'
            },
        'xrdb': 'Reread xresources file ("~/.Xresources").',
        'togglefloating': 'Toggle between floating and tiled.',
        'togglefloating,setsizeloc,tag': 'Make window dialog',
        'incnmaster': {
            '.i = -1': 'Decrease number of client in master area.',
            '.i = 1' : 'Increase number of client in master area.'
            },
        'setmfact': {
            '.f = 0.1': 'Increase master area size 10%',
            '.f = -0.1': 'Decrease master area size 10%'
        }
    }

keys = list()
reading = False
for line in lines:
    if line.startswith("static Key keys[]"):
        reading = True
    elif reading:
        if line.startswith('};'):
            break
        if 'modifier' not in line and 'argument' not in line and 'TAGKEYS' not in line:
            keys.append(line)

for i, keyline in enumerate(keys):
    if '/*' in keyline:
        keys[i] = keys[i].replace('/*', '')
        keys[i] = keys[i].replace('*/', '')
        keys[i] = keys[i].strip()
        continue
    elif '//' in keyline:
        keys[i] = keys[i].strip()
        keys[i] = keys[i].replace('//', '')
        continue
    keyline = keyline.replace('{', '')
    keyline = keyline.replace('}', '')
    keyline = keyline.replace(',', '')
    keys[i] = keyline.split()
    keys[i] = keys[i][:3] + [' '.join(keys[i][3:])]

# join multiples
okeys = keys
keys = list()
for kl in okeys:
    try:
        modmask, key, command, args = kl
        keys.append(kl)
    except ValueError:
        if isinstance(kl, str):
            keys.append([kl, '', '', ''])

# sort out keys+mods
for i, kl in enumerate(keys):
    modmask, key, command, args = kl
    if command == '' and args == '': 
        keys[i] = kl[:3]
        continue
    if 'XF86XK_' in key:
        key = key.replace('XF86XK_', '')
        key = re.sub(r'([A-Z])', r' \1', key)
    else:
        key = keydic[key]
    mods = [keydic[m] for m in modmask.split('|') if m != '0']
    mods = list(reversed(mods))
    mods.append(key)
    key = '-'.join(mods)
    if args == '0':
        args = ''
    try:
        args = vardic[args.split()[-1]]
    except:
        pass
    keys[i] = key, command, args

okeys = keys
keys = list()
for kl in okeys:
    key, command, args = kl
    if command == '' and args == '':
        keys.append(kl)
        continue
    if key in (justkeys:=list(list(zip(*keys))[0])):
        i = justkeys.index(key)
        if isinstance(keys[i], tuple):
            keys[i] = list(keys[i])
        if isinstance(keys[i][1], str):
            keys[i][1] = [keys[i][1]]
        if isinstance(keys[i][2], str):
            keys[i][2] = [keys[i][2]]
        keys[i][1].append(command)
        keys[i][2].append(args)
    else:
        keys.append(kl)

# sort out commands
for i, kl in enumerate(keys):
    key, command, args = kl
    if command == '' and args == '': 
        continue
    # deal with some specifics
    if command == 'spawn':
        pretty_args = args.replace('{', '')
        pretty_args = pretty_args.replace('}', '')
        pretty_args = pretty_args.replace('NULL', '')
        pretty_args = pretty_args.replace(';', '')
        for k, v in vardic.items():
            if k in pretty_args:
                pretty_args = pretty_args.replace(k, v)
        pretty_args = '"'+' '.join(eval('['+pretty_args+']'))+'"'
    elif command == 'focusdir':
        pretty_args = ['up', 'right', 'down', 'left'][int(args.split()[-1])-1]
    else:
        pretty_args = ''

    # tidy up names
    if isinstance(command, list):
        command = ','.join(command)
    if isinstance(args, list):
        args = ','.join([f'"{arg}"' for arg in args])
    try:
        cname = descdic[command]
        if isinstance(cname, dict):
            cname = cname[args]
        command = cname
    except KeyError:
        print(command, '"'+args+'"')
        pass
    keys[i] = (key, command, pretty_args)

lines = list()
for key, command, args in keys:
    if command == '' and args == '':
        lines.append(key)
    else:
        lines.append(key)
        lines.append(f' : {command} {args}\n')
key_lines = lines

## OUTPUT
with open("readme.md") as rdmf:
    lines = [l[:-1] for l in rdmf.readlines()]

header_lines = list()
footer_lines = list()
header = True
footer = False
for line in lines:
    if header:
        if line.lower().startswith('# key'):
            header = False
        header_lines.append(line)
    elif footer:
        footer_lines.append(line)
    else:
        if line.startswith('# '):
            footer = True
            footer_lines.append(line)

with open("readme.md", "w") as rdmf:
    for line in header_lines:
        rdmf.write(line+'\n')
    for line in key_lines:
        rdmf.write(line+'\n')
    for line in footer_lines:
        rdmf.write(line+'\n')

