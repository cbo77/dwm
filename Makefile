# dwm - dynamic window manager
# See LICENSE file for copyright and license details.

include mk_config
SRC = drw.c dwm.c util.c
OBJ = ${SRC:.c=.o}
GITVER=${shell git --no-pager log -1 | head -n 1 | cut -d ' ' -f 2 | cut -c1-7}

all: man dwm

options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h mk_config

dwm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f dwm ${OBJ} dwm-${VERSION}.tar.gz

man: config.h readme.md
	python keyconfig.py
	pandoc -t man -s readme.md -o dwm.1

install: all
	sudo mkdir -p ${DESTDIR}${PREFIX}/bin
	sudo cp -f dwm ${DESTDIR}${PREFIX}/bin
	sudo chmod 755 ${DESTDIR}${PREFIX}/bin/dwm
	sudo mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cat dwm.1 | sed "s/VERSION/${VERSION}/g" | sed "s/GITVER/${GITVER}/g" | sudo tee ${DESTDIR}${MANPREFIX}/man1/dwm.1 >/dev/null
	sudo chmod 644 ${DESTDIR}${MANPREFIX}/man1/dwm.1

uninstall:
	sudo rm -f ${DESTDIR}${PREFIX}/bin/dwm\
		${DESTDIR}${MANPREFIX}/man1/dwm.1

.PHONY: all options clean install uninstall
