---
title: dwm(1) vVERSION (rev GITVER)
author:
- Chris Boyle
- the folks at suckless.org
date: April 2019
---

# NAME

dwm - dynamic window manager (cbo77 fork)

# DESCRIPTION

[dwm](https://dwm.suckless.org) is an extremely fast, small, and dynamic 
window manager for X.

cbo77's config. Featuring: dynamic geometry to show selection, xresource file for
theming, some handy media keybinds.

# KEYBINDS
## General

shift-super-q
 : Quit dwm. 

super-q
 : Close selected window. 

super-p
 : Move client to master area. 

super-b
 : Toggle bar on current screen. 

super-tab
 : View last visited tag 

super-a
 : View all tags 

super-F5
 : Reread xresources file ("~/.Xresources"). 

## Floating clients

super-f
 : Toggle between floating and tiled. 

shift-super-f
 : Make window dialog 

## Spawning commands

super-d
 : Run command:  "dmenu_run -m 0 -p run:"

super-u
 : Run command:  "dmenu_umnt -m 0 -p unmount:"

super-z
 : Run command:  "/bin/sh -c $TERMCMD"

super-w
 : Run command:  "firefox"

super-s
 : Run command:  "slock"

super-m
 : Run command:  "fman"

## Changing focus

super-k
 : Change focus  up

super-l
 : Change focus  right

super-j
 : Change focus  down

super-h
 : Change focus  left

## Master area

shift-super-h
 : Decrease number of client in master area. 

shift-super-l
 : Increase number of client in master area. 

shift-super-k
 : Increase master area size 10% 

shift-super-j
 : Decrease master area size 10% 

super-grave
 : Toggle focussed screen. 

shift-super-grave
 : Move client between screens. 

## Media

Media keys are handy for controlling mpd, without the need to open an mpd
client. There are two general schema for this; one using the XF86 media keys,
which are handy if the keyboard has them, and the other using the v key.

super-v
 : Run command:  "/usr/bin/pactl set-sink-volume 0 +5%"

shift-super-v
 : Run command:  "/usr/bin/pactl set-sink-volume 0 -5%"

control-super-v
 : Run command:  "/usr/bin/mpc toggle"

 Audio Lower Volume
 : Run command:  "/usr/bin/pactl set-sink-volume 0 -5%"

 Audio Mute
 : Run command:  "/usr/bin/pactl set-sink-mute 0 toggle"

 Audio Raise Volume
 : Run command:  "/usr/bin/pactl set-sink-volume 0 +5%"

 Audio Next
 : Run command:  "/usr/bin/mpc next"

 Audio Prev
 : Run command:  "/usr/bin/mpc prev"

 Audio Play
 : Run command:  "/usr/bin/mpc toggle"

 Audio Pause
 : Run command:  "/usr/bin/mpc toggle"

 Audio Stop
 : Run command:  "/usr/bin/mpc toggle"

## Tags

There are N tags in a dwm session, where N is an integer 1-9. The tags can
representing one of the tags.

 super-X
  : Focus only tag X.

 control-super-X
  : Add tag X to the focus.

 shift-super-X
  : Move window to tag X.

 control-shift-super-X
  : Copy window to tag X.

